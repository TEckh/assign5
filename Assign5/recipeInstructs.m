//
//  ViewController+recipeInstructs.m
//  Assign5
//
//  Created by Trevor Eckhardt on 6/11/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "recipeInstructs.h"
#import "CustomObject.h"

@implementation UIViewController (recipeInstructs)

-(void)viewDidLoad{
    //[super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRecieved:) name:@"recipeChanged" object:nil];
    
//    UIView *headerView = [UIView new];
//    headerView.backgroundColor = [UIColor lightGrayColor];
//    [self.view addSubview:headerView];
//    
//    UILabel *headline = [UILabel new];
//    headline.font = [UIFont systemFontOfSize:30.0];
//    headline.numberOfLines = 0;
//    headline.preferredMaxLayoutWidth = 125.0; // This is necessary to allow the label to use multi-line text properly
//    headline.backgroundColor = [UIColor yellowColor]; // So we can see the sizing
//    headline.text = @"Kitten kills again";
//    [headerView addSubview:headline];
//    
//    UILabel *byline = [UILabel new];
//    byline.font = [UIFont systemFontOfSize:14.0];
//    byline.backgroundColor = [UIColor greenColor]; // So we can see the sizing
//    byline.text = @"Paws McGruff";
//    
//    [headerView addSubview:byline];
//    
//    UIImageView *imageView = [UIImageView new];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [headerView addSubview:imageView];
//    
//    // Lovely image loaded for example purposes
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        UIImage *kitten = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://placekitten.com/150/150"] options:0 error:nil]];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            imageView.image = kitten;
//        });
//    });
//    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    button.translatesAutoresizingMaskIntoConstraints = NO;
//    [button setTitle:@"Button" forState:UIControlStateNormal];
//    [headerView addSubview:button];
//    
//    // Layout
//    
//    NSDictionary *views = NSDictionaryOfVariableBindings(headerView,headline,byline,imageView,button);
//    NSDictionary *metrics = @{@"imageEdge":@150.0,@"padding":@15.0};
//    
//    // Header view fills the width of its superview
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[headerView]|" options:0 metrics:metrics views:views]];
//    // Header view is pinned to the top of the superview
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[headerView]" options:0 metrics:metrics views:views]];
//    
//    // Headline and image horizontal layout
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[headline]-padding-[imageView(imageEdge)]-padding-|" options:0 metrics:metrics views:views]];
//    
//    // Headline and byline vertical layout - spacing at least zero between the two
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[headline]->=0-[byline]-padding-|" options:NSLayoutFormatAlignAllLeft metrics:metrics views:views]];
//    
//    // Image and button vertical layout - spacing at least 15 between the two
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[imageView(imageEdge)]->=padding-[button]-padding-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:metrics views:views]];
}

-(void)notificationReceived:(NSNotification*)n
{
    CGFloat height = self.view.frame.size.height - 65;
    
    UIView *headerView = [UIView new];
    headerView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:headerView];
    
    UILabel *headline = [UILabel new];
    headline.font = [UIFont systemFontOfSize:30.0];
    headline.numberOfLines = 0;
    headline.preferredMaxLayoutWidth = 125.0; // This is necessary to allow the label to use multi-line text properly
    headline.backgroundColor = [UIColor yellowColor]; // So we can see the sizing
    headline.text = [@[n.object objectForKey:@"Name"]];
    [headerView addSubview:headline];
    
//    UILabel *byline = [UILabel new];
//    byline.font = [UIFont systemFontOfSize:14.0];
//    byline.backgroundColor = [UIColor greenColor]; // So we can see the sizing
//    byline.text = @"Paws McGruff";
//    
//    [headerView addSubview:byline];
    
    UIImageView *imageView = [UIImageView new];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [headerView addSubview:imageView];
    
    // Lovely image loaded for example purposes
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *recipeImage = [UIImage imageWithData:[[n.object objectForKey:@"Image"] options:0 error:nil]];
        dispatch_async(dispatch_get_main_queue(), ^{
            imageView.image = recipeImage;
        });
    });
    
    UITextView* labelDesc = [[UITextView alloc]initWithFrame:CGRectMake(0, 65 + (height * 24/48), self.view.frame.size.width, (height * 23/48))];
    labelDesc.text = [n.object desc];
    labelDesc.textAlignment = NSTextAlignmentLeft;
    labelDesc.textColor = [UIColor blackColor];
    labelDesc.editable = NO;
    [headerView addSubview:labelDesc];
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    button.translatesAutoresizingMaskIntoConstraints = NO;
//    [button setTitle:@"Button" forState:UIControlStateNormal];
//    [headerView addSubview:button];
    
    // Layout
    
//    NSDictionary *views = NSDictionaryOfVariableBindings(headerView,headline,byline,imageView,button);
//    NSDictionary *metrics = @{@"imageEdge":@150.0,@"padding":@15.0};
//    
//    // Header view fills the width of its superview
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[headerView]|" options:0 metrics:metrics views:views]];
//    // Header view is pinned to the top of the superview
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[headerView]" options:0 metrics:metrics views:views]];
//    
//    // Headline and image horizontal layout
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[headline]-padding-[imageView(imageEdge)]-padding-|" options:0 metrics:metrics views:views]];
//    
//    // Headline and byline vertical layout - spacing at least zero between the two
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[headline]->=0-[byline]-padding-|" options:NSLayoutFormatAlignAllLeft metrics:metrics views:views]];
//    
//    // Image and button vertical layout - spacing at least 15 between the two
//    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[imageView(imageEdge)]->=padding-[button]-padding-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:metrics views:views]];
}

@end
