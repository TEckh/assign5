//
//  ViewController.m
//  Assign5
//
//  Created by Trevor Eckhardt on 6/11/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "ViewController.h"
#import "CustomObject.h"
#import "MyCell.h"
#import "recipeInstructs.h"

@interface ViewController ()

@end

@implementation ViewController

NSArray* recipes;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    NSArray* temp = [[NSArray alloc] initWithContentsOfFile:filePath];
    NSMutableArray* rec = [NSMutableArray new];
    
    for (NSDictionary* a in temp)
    {
        CustomObject* cust = [CustomObject new];
        cust.name = [a objectForKey:@"Name"];
        cust.desc = [a objectForKey:@"Description"];
        cust.image = [a objectForKey:@"Image"];
        
        [rec addObject:cust];
    }
    
    recipes = [[NSArray alloc] initWithArray:rec copyItems:NO];
    
    UICollectionViewFlowLayout* fl = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView* cv = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) collectionViewLayout:fl];
    
    cv.delegate = self;
    cv.dataSource = self;
    
    [cv registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:cv];

    
    // Do any additional setup after loading the view, typically from a nib.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MyCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor blueColor];
//    UIImageView* recipeImage = [UIImageView new];
//    recipeImage.image = [UIImage imageNamed:[[recipes objectAtIndex:indexPath.row] image]];
    
    UILabel* lbl = [UILabel new];
    lbl.text = [[recipes objectAtIndex:indexPath.row] name];
    
    //cell.image = recipeImage;
    cell.label = lbl;
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width / 2 - 20, 200);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recipeChanged" object:[recipes objectAtIndex:indexPath.row]];
    
    recipeInstructs* info = [recipeInstructs new];
    
    //info.informationDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
    
    //Put notification here to transfer over to cvc.
    NSLog(@"IndexPath: %@", indexPath);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 5);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
