//
//  ViewController.h
//  Assign5
//
//  Created by Trevor Eckhardt on 6/11/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout>


@end

